<?php

namespace App\Http\Controllers\Cashier;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CashierCartsController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $Carts = Cart::with('user', 'product')->where('user_id', Auth::user()->id)->where('transaction_id', null)->get();
        $Subtotal = $Carts->sum('sub_total');
        return view('cashier.cart', compact('Carts', 'Subtotal'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(String $id)
    {
        try {
            $product = Product::find($id);
            if ($product) {
                $cart = Cart::where('user_id', Auth::user()->id)->where('transaction_id', null)->where('product_id', $id)->get();
                if (count($cart) == 0) {
                    $cart = Cart::create(
                        [
                            "user_id" => Auth::user()->id ?? '2',
                            "product_id" => $product->id,
                            "qty" => 1,
                            "price" => $product->price,
                            "sub_total" => $product->price * 1
                        ]
                    );
                } else {
                    return redirect()->route('cashier.products')->with('success', 'product sudah ada di carts');
                }
                return redirect()->route('cashier.products')->with('success', 'Data Successfully Added');
            } else {
                return redirect()->route('cashier.products')->with('error', 'Product not found');
            }
        } catch (Exception $e) {
            return redirect()->route('cashier.products')->with('error', 'error: ' . $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            $Carts = Cart::findOrFail($id);
            $validatedData = $request->validate(
                [
                    "qty" => 'required'
                ]
            );
            $qty = $validatedData['qty'];
            if ($qty  == '0') {
                $Carts->delete();
                return redirect()->route('cashier.carts')->with('success', 'data successfully deleted');
            } else {
                $Carts->qty = $qty;
                $Carts->sub_total = $Carts->price * $qty;
                $Carts->save();
                return redirect()->route('cashier.carts')->with('success', 'data successfully updated');
            }
        } catch (Exception $e) {
            return redirect()->route('cashier.carts')->with('error', 'error occurred' . $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}