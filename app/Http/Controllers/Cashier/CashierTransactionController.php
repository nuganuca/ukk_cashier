<?php

namespace App\Http\Controllers\Cashier;

use App\Http\Controllers\Controller;
use App\Models\Cart;
use App\Models\Product;
use App\Models\Transaction;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CashierTransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $Transaction = Transaction::with('user', 'carts')->get();
        return view('cashier.transaction', compact('Transaction'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        try {
            $Carts = Cart::with('user', 'product')->where('user_id', Auth::user()->id)->where('transaction_id', null)->get();
            $transaction = Transaction::all()->sortByDesc('serial_number')->first();
            $validatedData = $request->validate(
                ['paid' => 'required|numeric']
            );
            $serial_number = $transaction != null ? $transaction->serial_number + 1 : 1;
            $paid = $validatedData['paid'];
            $Subtotal = $Carts->sum('sub_total');
            $change = $paid - $Subtotal;
            if ($change < 0) {
                return redirect()->route('cashier.carts')->with('error', 'your transaction is invalid');
            }
            $userId = Auth::user()->id;
            if (count($Carts) != 0) {
                foreach ($Carts as $c) {
                    $Product = Product::find($c->product_id);
                    if ($Product) {
                        $stock = $Product->stock - $c->qty;
                        if ($stock < 0) {
                            return redirect()->route('cashier.carts')->with('error', 'stock not enough');
                        }
                        if ($stock == 0) {
                            $Product->is_active = false;
                            $Product->save();
                        }
                        $Product->stock -= $c->qty;
                        $Product->save();
                    }
                }
                $newTransaction = Transaction::create([
                    'user_id' => $userId,
                    'serial_number' => $serial_number,
                    'sub_total' => $Subtotal,
                    'paid' => $paid,
                    'total' => $Subtotal,
                    'change' => $change
                ]);
                Cart::whereNull('transaction_id')->where('user_id', Auth::user()->id)->update(['transaction_id' => $newTransaction->id]);
                return view('cashier.transaction.detail-page', ['Transaction' => $newTransaction]);
            } else {
                return redirect()->route('cashier.carts')->with('error', 'carts is empty');
            }
        } catch (Exception $e) {
            return redirect()->route('cashier.carts')->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        $Transaction = Transaction::with('user')->findOrFail($id);
        $Cart = Cart::with('user', 'product')->where('transaction_id', $id)->get();
        return view('admin.dashboard.detail-page', compact('Transaction', 'Cart'));
    }


    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}