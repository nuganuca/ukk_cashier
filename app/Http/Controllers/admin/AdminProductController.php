<?php

namespace App\Http\Controllers\admin;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Exception;
use Illuminate\Http\Request;

class AdminProductController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        $product = Product::all();
        return view('admin.products', compact('product'));
    }

    /**
     * Show the form for creating a new resource.
     */
    public function create()
    {
        return view('admin.product.form-tambah');
    }

    /**
     * Store a newly created resource in storage.
     */
    public function store(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string',
            'stock' => 'required|integer',
            'price' => 'required|numeric',
        ]);
        try {
            Product::create($validatedData);
            return redirect()->route('admin.products')->with('success', 'Successfully created');
        } catch (Exception $e) {
            return redirect()->to('admin.products.create')->with('error', $e->getMessage());
        }
    }

    /**
     * Display the specified resource.
     */
    public function show(Request $request)
    {
        $validatedData = $request->validate([
            'query' => '',
        ]);
        $query = $validatedData['query'];
        try {
            $product = Product::where('name', "like", "%" . $query . "%")->get();
            return view('admin.products', compact('product'))->with("success", "data found successfully");
        } catch (Exception $e) {
            return redirect()->route('admin.products');
        }
    }

    /**
     * Show the form for editing the specified resource.
     */
    public function edit(string $id)
    {
        try {
            $products = Product::find($id);
            return view('admin.product.form-edit', compact('products'));
        } catch (exception $e) {
            return redirect()->route('admin.products')->with('error', $e->getMessage());
        }
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        try {
            $data = Product::find($id);
            $validatedData = $request->validate([
                'name' => 'required|string',
                'stock' => 'required|integer',
                'price' => 'required|numeric',
            ]);
            $data->name = $validatedData["name"];
            $data->stock = $validatedData["stock"];
            $data->price = $validatedData["price"];
            $data->save();
            return redirect()->route('admin.products')->with('success', "data updated successfully");
        } catch (exception $e) {
            return redirect()->route('admin.products')->with('error', $e->getMessage());
        }
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        try {
            $Product = Product::find($id);
            $Product->is_active = false;
            $Product->save();
            return redirect()->route('admin.products')->with('success', 'Successfully deleted');
        } catch (Exception $e) {
            return redirect()->route('admin.products')->with('error', $e->getMessage());
        }
    }
    public function activate(string $id)
    {
        try {
            $Product = Product::find($id);
            $Product->is_active = true;
            $Product->save();
            return redirect()->route('admin.products')->with('success', 'Successfully activated');
        } catch (Exception $e) {
            return redirect()->route('admin.products')->with('error', $e->getMessage());
        }
    }
}