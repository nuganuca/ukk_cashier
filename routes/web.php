<?php

use App\Http\Controllers\admin\AdminDashboardController;
use App\Http\Controllers\admin\AdminProductController;
use App\Http\Controllers\admin\AdminUserController;
use App\Http\Controllers\Cashier\CashierCartsController;
use App\Http\Controllers\Cashier\CashierProductsController;
use App\Http\Controllers\Cashier\CashierTransactionController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ProfileController;
use Illuminate\Support\Facades\Route;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "web" middleware group. Make something great!
|
*/

Route::get('/', function () {
    return redirect()->to('login');
})->name('welcome');
Route::get('/home', [HomeController::class, "index"])->name('home');

Route::middleware(['role:1'])->prefix('/admin')->group(function () {
    Route::controller(AdminProductController::class)->prefix('/products')->group(function () {
        Route::get('/', "index")->name('admin.products');
        Route::get('/create', "create")->name('admin.products.create');
        Route::post('/store', "store")->name('admin.products.store');
        Route::get('/edit/{id}', "edit")->name('admin.products.edit');
        Route::post('/update/{id}', "update")->name('admin.products.update');

        Route::get('/destory/{id}', "destroy")->name('admin.products.destroy');
        Route::get('/activate/{id}', "activate")->name('admin.products.activate');

        Route::post('/find', "show")->name('admin.products.show');
    });

    Route::controller(AdminUserController::class)->prefix('/users')->group(function () {
        Route::get('/', "index")->name('admin.user');
        Route::get('/create', "create")->name('admin.user.create');
        Route::post('/store', "store")->name('admin.user.store');
    });
    Route::controller(AdminDashboardController::class)->prefix('/dashboard')->group(function () {
        Route::get('/', 'index')->name('admin.dashboard');
        Route::get('/details/{id}', 'show')->name('admin.dashboard.details');
    });
});
Route::middleware(['role:0'])->prefix('/cashier')->group(function () {
    Route::controller(CashierProductsController::class)->prefix('/products')->group(function () {
        Route::get('/',  "index")->name('cashier.products');
        Route::post('/find',  "show")->name('cashier.products.show');
    });
    Route::controller(CashierCartsController::class)->prefix('/carts')->group(function () {
        Route::get('/', 'index')->name('cashier.carts');
        Route::get('/store/{id}', 'store')->name('cashier.carts.store');
        Route::post('/update/{id}', "update")->name('cashier.carts.update');
    });
    Route::controller(CashierTransactionController::class)->prefix('/transactions')->group(function () {
        Route::get('/', 'index')->name('cashier.transactions');
        Route::post('/store', 'store')->name('cashier.transactions.store');
        Route::get('/details/{id}', 'show')->name('cashier.transactions.details');
    });
});
Route::middleware('auth')->controller(ProfileController::class)->group(function () {
    Route::get('/profile', 'edit')->name('profile.edit');
    Route::patch('/profile', 'update')->name('profile.update');
    Route::delete('/profile', 'destroy')->name('profile.destroy');
});

require __DIR__ . '/auth.php';