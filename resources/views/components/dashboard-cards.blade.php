<div {{ $attributes->merge([ 'class' => "md:h-32 h-28 bg-white rounded-lg shadow-sm shadow-gray-400 col-span-1 p-4"]) }}>
    {{$slot}}
</div>