@props(['no', 'name', 'qty', 'price'])
<li class="flex justify-center">
    <h1 class="w-12 text-center">{{ $no }}</h1>
    <h1 class="flex-1 text-center">{{ $name }}</h1>
    <h1 class="flex-1 text-center">{{ $qty }}</h1>
    <h1 class="flex-1 text-center">{{ $price }}</h1>
</li>
