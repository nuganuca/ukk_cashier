<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __("Products") }}
        </h2>
    </x-slot>

    <div class="py-6">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8 ">
            <div class="w-full flex justify-end mb-2 join"> 
            <form class="flex flex-shrink gap-2" action="{{route('admin.products.show')}}" method="POST" enctype="multipart/form-data">
                @csrf
               <div class="join">
                 <x-text-input id="query" name="query" type="text" placeholder="Search here ..." class=" w-full input input-bordered bg-white"   autocomplete="query"/>
                <button class="btn btn-info max-h-min min-h-min rounded-r-md" type="submit">search</button>
               </div>
            </form>
            <a class="btn-info btn ms-auto text-white" href="{{route("admin.products.create")}}">
                tambah
            </a>
            </div>
            <div class="bg-white overflow-y-scroll max-h-[26rem] shadow-sm sm:rounded-lg p-4">
                <table class="table text-black border-none border-collapse">
                    <thead>
                        <tr class="text-black border-none border-collapse">
                            <th>No</th>
                            <th>Name</th>
                            <th>Stock</th>
                            <th>Price</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($product as $p)
                        <tr @class(['border-none border-collapse','bg-white'=>$loop->iteration%2==0, 'bg-gray-100' => $loop->iteration%2==1, 'bg-red-100'=>!$p->is_active] )>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$p->name}}</td>
                            <td>{{$p->stock}}</td>
                            <td>{{$p->price}}</td>
                            <td class="flex justify-center gap-2">
                                <a class="btn btn-warning max-w-min min-w-min" href="{{route('admin.products.edit', ['id'=>$p->id])}}">edit</a>
                                @if(!$p->is_active)
                                    <a class="btn btn-error max-w-min min-w-min" href="{{route('admin.products.activate', ['id'=>$p->id])}}" onclick="()=>confirm('apakah anda yakin?')">activate</a>
                                @else
                                    <a class="btn btn-error max-w-min min-w-min" href="{{route('admin.products.destroy', ['id'=>$p->id])}}" onclick="()=>confirm('apakah anda yakin?')">hapus</a>
                                @endif
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>
