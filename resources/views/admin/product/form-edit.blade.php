<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __("Form Edit") }}
        </h2>
    </x-slot>

    <div class="py-6">
        <div class="max-w-7xl max-h-96 overflow-y-scroll mx-auto sm:px-6 lg:px-8 ">
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg p-4">
                <form method="POST" enctype="multipart/form-data" action="{{route("admin.products.update", ["id"=>$products->id])}}">
                    {{ csrf_field() }}
                    <x-text-input id="name" name="name" type="text" placeholder="name" class="mt-2 block w-full" value="{{$products->name}}"  required autofocus autocomplete="name"/>
                    <x-text-input id="stock" name="stock" type="number" placeholder="stock" class="mt-2 block w-full" value="{{$products->stock}}"  required autofocus autocomplete="stock"/>
                    <x-text-input id="price" name="price" type="number" placeholder="price" class="mt-2 block w-full" value="{{$products->price}}" required autofocus autocomplete="price"/>
                    <button type="submit" class="btn btn-primary mt-2 text-white">
                        submit
                    </button>
                </form>
            </div>
        </div>
    </div>
</x-app-layout>
