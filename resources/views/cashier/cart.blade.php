<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('Dashboard') }}
        </h2>
    </x-slot>

    <div class="py-6">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-y-scroll h-96 shadow-sm sm:rounded-lg p-4 text-black">
                <ul>
                    @foreach ($Carts as $c)
                        <li  @class(['flex p-1', 'bg-white'=>$loop->iteration%2==0, 'bg-gray-100' => $loop->iteration%2==1, 'bg-red-100'=>!$c->product->is_active])>
                        <div class="block flex-1 text-start">
                            <h1>
                                {{$c->product->name."  ".$c->qty."x"}}
                            </h1>
                            <p>
                                {{$c->price}}
                            </p>
                        </div>
                        <div class="flex-1 text-center my-auto">
                            {{$c->sub_total}}
                        </div>
                        <div class="flex-1 flex justify-end my-auto">
                            <form class="end-0" action="{{route('cashier.carts.update', ['id'=>$c->id])}}" enctype="multipart/form-data" method="POST">
                                @csrf
                            <div class="join">
                                <x-text-input id="qty" name="qty" type="text" placeholder="Search here ..." value="{{$c->qty}}" class="text-center text-sm w-28 h-10 input input-bordered bg-white"   autocomplete="query"/>
                                <button class="btn btn-info text-white max-h-min min-h-min rounded-r-md h-10" type="submit">tambah</button>
                            </div>
                            </form>
                        </div>
                    </li>
                    @endforeach
                </ul>
            </div>
            <div class="bg-white overflow-hidden shadow-sm sm:rounded-lg p-4 text-black my-2">
                <ul>
                    <li class="text-lg text-black font-bold flex ">
                        <div class="flex-1 my-auto">
                            Total Transaksi
                        </div>
                    </li>
                    <li class="text-md text-black flex">
                        <div class="flex-1 flex justify-start my-auto">
                            Rp. {{$Subtotal}}
                        </div>
                        <div class="flex-1 flex justify-end">
                            <form class="end-0" action="{{route('cashier.transactions.store')}}" enctype="multipart/form-data" method="POST">
                                @csrf
                            <div class="join">
                                <x-text-input id="qty" name="paid" type="text" placeholder="Customer Paid" class="text-center text-sm w-36 h-10 input input-bordered bg-white"   autocomplete="query"/>
                                <button class="btn btn-info text-white max-h-min min-h-min rounded-r-md h-10" type="submit">Checkout</button>
                            </div>
                            </form>
                        </div>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</x-app-layout>
