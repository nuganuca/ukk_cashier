<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __('History Transaction') }}
        </h2>
    </x-slot>

    <div class="py-6">
        <div class="max-w-7xl mx-auto sm:px-6 lg:px-8">
            <div class="bg-white overflow-y-scroll shadow-sm sm:rounded-lg p-4  h-[26rem]">
                 <table class="table text-black border-none border-collapse">
                    <thead>
                        <tr class="text-black border-none border-collapse">
                            <th>No</th>
                            <th>Serial Number</th>
                            <th>Cashier</th>
                            <th>Products</th>
                            <th>Total</th>
                            <th class="text-center">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach ($Transaction as $t)
                        <tr @class(['border-none border-collapse','bg-white'=>$loop->iteration%2==0, 'bg-gray-100' => $loop->iteration%2==1])>
                            <td>{{$loop->iteration}}</td>
                            <td>{{$t->serial_number}}</td>
                            <td>{{$t->user->name}}</td>
                            <td>{{count($t->carts)}}</td>
                            <td>{{$t->total}}</td>
                            <td class="flex justify-center"><a class="btn btn-warning text-white " href="{{route('cashier.transactions.details', ['id'=>$t->id])}}">details</a></td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</x-app-layout>
