<x-app-layout>
    <x-slot name="header">
        <h2 class="font-semibold text-xl text-gray-800 leading-tight">
            {{ __("Detail Transaction") }}
        </h2>
    </x-slot>

    <div class="py-6">
        <div class="max-w-7xl max-h-96 overflow-y-scroll mx-auto sm:px-6 lg:px-8 ">
            <div class="bg-white  shadow-sm sm:rounded-lg p-4">
                <ul class="text-black">
                    <li class="flex">
                        <h1 class="flex-1 font-bold">
                            Serial Number:
                        </h1>
                        <p class="flex-1 text-end">
                            {{$Transaction->serial_number}}
                        </p>
                    </li>
                    <li class="flex">
                        <h1 class="flex-1 font-bold">
                            Subtotal:
                        </h1>
                        <p class="flex-1 text-end">
                            {{$Transaction->sub_total}}
                        </p>
                    </li>
                    <li class="flex">
                        <h1 class="flex-1 font-bold">
                            Paid:
                        </h1>
                        <p class="flex-1 text-end">
                            {{$Transaction->paid}}
                        </p>
                    </li>
                    <li class="flex">
                        <h1 class="flex-1 font-bold">
                            Change
                        </h1>
                        <p class="flex-1 text-end">
                            {{$Transaction->change}}
                        </p>
                    </li>
                    <li class="flex">
                        <h1 class="flex-1 font-bold">
                            Cashier:
                        </h1>
                        <p class="flex-1 text-end">
                            {{$Transaction->user->name}}
                        </p>
                    </li>
                    <li class="border-2 border-dashed border-black my-2">
                    </li>
                    @foreach ($Transaction->carts as $c)
                        <li class="flex">
                        <div class="block flex-1 text-start">
                            <h1>
                                {{$c->product->name."  ".$c->qty."x"}}
                            </h1>
                            <p>
                                {{$c->price}}
                            </p>
                        </div>
                        <div class="flex-1 text-end">
                            {{$c->sub_total}}
                        </div>
                    </li>
                    @endforeach
                    <li class="border-2 border-black border-dashed my-2">

                    </li>
                    <li class="flex">
                        <h1 class="flex-1 font-bold">
                            Total:
                        </h1>
                        <p class="flex-1 text-end">
                            {{$Transaction->total}}
                        </p>
                    </li>
                    <li>
                        <a class="btn btn-warning" href="{{route('cashier.transactions')}}">
                            kembali
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</x-app-layout>
